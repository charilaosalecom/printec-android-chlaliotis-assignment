package com.chlaliotis.printecusecase.viewmodels

import android.util.Log
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.chlaliotis.printecusecase.data.model.Payment
import com.chlaliotis.printecusecase.data.model.PaymentFormState
import com.chlaliotis.printecusecase.preferences.BASE_URL
import com.chlaliotis.printecusecase.preferences.PreferencesHelper
import com.chlaliotis.printecusecase.repositories.PayRepository
import javax.inject.Inject

class SettingsViewModel  @ViewModelInject constructor(private val preferencesHelper: PreferencesHelper,@Assisted private val savedStateHandle: SavedStateHandle) :
    ViewModel(), Observable {



    @Bindable
     val minAmount = MutableLiveData<String>()



    @Bindable
    val maxAmount = MutableLiveData<String>()

    @Bindable
    val baseUrl = MutableLiveData<String>()

    val checkValues = MutableLiveData<Boolean>()



    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        Log.i("BINDING", "add property callback")


    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        Log.i("", "remove callback")
    }

   fun checkvalues() {
       try {
           if (minAmount.value?.toInt()!! < maxAmount.value?.toInt()!!) {
               checkValues.value = true
           } else {
               checkValues.value = false
           }
       } catch (exception: NumberFormatException) {
           checkValues.value = false
       }
   }


}
