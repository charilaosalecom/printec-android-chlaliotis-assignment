package com.chlaliotis.printecusecase.networking

import com.chlaliotis.printecusecase.data.model.Payment
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface UserService {

    @POST("api/printec/financial/pay")
    suspend fun pay(@Body payRequest: PayRequest): Response<PayResult>


    @Headers("Content-Type: application/json")
    @POST("/api/printec/financial/refund/receipt-number")
    suspend fun receipt(@Body receiptRequest: ReceiptRequest): Response<Void>

    @POST("/api/printec/financial/refund")
    suspend fun refund(@Body refundRequest: RefundRequest): Response<Void>
}