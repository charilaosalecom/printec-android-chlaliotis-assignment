package com.chlaliotis.printecusecase.networking

data class ReceiptRequest(val receipt_number:String)