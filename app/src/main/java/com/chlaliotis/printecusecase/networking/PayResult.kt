package com.chlaliotis.printecusecase.networking

data class PayResult(val receiptNumber: String = "",val amount:String ="", val currency:String,val error: Int? = null)

