package com.chlaliotis.printecusecase.networking

data class RefundRequest(val receipt_number: String, val amount:String)