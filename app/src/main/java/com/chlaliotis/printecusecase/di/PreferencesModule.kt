package com.chlaliotis.printecusecase.di

import com.chlaliotis.printecusecase.preferences.PreferencesHelper
import com.chlaliotis.printecusecase.preferences.PreferencesHelperImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
abstract class PreferencesModule{

    @Binds
    @Singleton
    abstract fun bindPreferencesHelper(preferencesHelperImpl: PreferencesHelperImpl): PreferencesHelper

}