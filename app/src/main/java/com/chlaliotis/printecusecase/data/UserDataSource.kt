package com.chlaliotis.printecusecase.data

import android.util.Log
import com.chlaliotis.printecusecase.data.model.Payment
import com.chlaliotis.printecusecase.data.Result
import com.chlaliotis.printecusecase.networking.*
import org.json.JSONObject
import java.io.IOException
import javax.inject.Inject

class UserDataSource @Inject constructor(private val userService: UserService) {

    suspend  fun pay(amount: String): Result<Payment>{
        return try {

            val paymentresult =  userService.pay(PayRequest(amount)).body()


            return Result.Success(Payment(paymentresult!!.receiptNumber,paymentresult.amount,paymentresult.currency))
        } catch (e: Throwable) {
            Result.Error(IOException("Error paying", e))
        }
    }
    suspend  fun receipt(receipt_number: String): Result<String>{
        return try {

            val receiptResult =  userService.receipt(ReceiptRequest(receipt_number))
          //  if(receiptResult in 200..2002)return Result.Success("OK")
            if(receiptResult.code() in 200..202)return Result.Success("OK")

            return Result.Error(IOException("Not validated"))
        } catch (e: Throwable) {
            Result.Error(IOException("Error paying", e))
        }
    }
    suspend  fun refund(receipt_number: String,amount:String): Result<String>{
        return try {

            val receiptResult =  userService.refund(RefundRequest(receipt_number, amount))
            //  if(receiptResult in 200..2002)return Result.Success("OK")
            if(receiptResult.code() in 200..202){

                return Result.Success("OK")
            }

            return Result.Error(IOException("Not validated"))
        } catch (e: Throwable) {
            Result.Error(IOException("Error paying", e))
        }
    }


}