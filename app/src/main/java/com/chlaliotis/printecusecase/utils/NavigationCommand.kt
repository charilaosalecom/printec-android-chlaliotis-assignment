package com.chlaliotis.printecusecase.utils

import androidx.navigation.NavController

typealias NavigationCommand = (NavController) -> Unit
