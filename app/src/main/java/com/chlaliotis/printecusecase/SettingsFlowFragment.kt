package com.chlaliotis.printecusecase

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.chlaliotis.printecusecase.databinding.SettingBind2Binding
import com.chlaliotis.printecusecase.preferences.BASE_URL
import com.chlaliotis.printecusecase.preferences.PreferencesHelper
import com.chlaliotis.printecusecase.viewmodels.PaymentViewModel
import com.chlaliotis.printecusecase.viewmodels.SettingsViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.net.URL
import javax.inject.Inject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [SettingsFlowFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

@AndroidEntryPoint
class SettingsFlowFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private val viewModel by viewModels<SettingsViewModel>()
    private lateinit var binding: SettingBind2Binding
    @Inject
    lateinit var preferencehelper: PreferencesHelper


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //viewModel._baseUrl.value = BASE_URL
        val binding = SettingBind2Binding.inflate(layoutInflater, container, false)
        binding.viewModel = viewModel.apply {
            minAmount.value = preferencehelper.getMinAmount()
            maxAmount.value = preferencehelper.getMaxAmount()
            baseUrl.value = preferencehelper.getUrl()
        }




        binding.setLifecycleOwner(this)
        viewModel.minAmount.observe(viewLifecycleOwner, Observer { it ->
             viewModel.checkvalues()

        })

        viewModel.maxAmount.observe(viewLifecycleOwner, { it ->
            viewModel.checkvalues()

        })





        return binding.root


    }


    override fun onPause() {

        super.onPause()
        with(preferencehelper) {
            if(viewModel.checkValues.value==true){
            viewModel.baseUrl.value?.let { saveUrl(it) }
            viewModel.maxAmount.value?.let {
                (saveMaxAmount(it))
                viewModel.minAmount.value?.let { saveMinAmount(it) }

            }

        }}

    }
}